<?php

namespace Drupal\confetti\Form;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;

/**
 * Summary of ConfettiSettingsForm.
 */
class ConfettiSettingsForm extends ConfigFormBase {


    /**
     * The storage handler class for nodes.
     *
     * @var \Drupal\node\NodeStorage
     */
    protected $nodeStorage;

    /**
     * The cache tags invalidator.
     *
     * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
     */
    protected $cacheTagsInvalidator;

    /**
     * Class constructor.
     *
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity
     *   The Entity type manager service.
     */
    public function __construct(EntityTypeManagerInterface $entity, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
        $this->nodeStorage = $entity->getStorage('node');
        $this->cacheTagsInvalidator = $cache_tags_invalidator;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('entity_type.manager'),
            $container->get('cache_tags.invalidator')
        );
    }
    /**
     * GetFormId.
     */
    public function getFormId() {
        return 'confetti_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'confetti.settings',
        ];
    }

    /**
     * BuildForm.
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        // Get the confetti settings from configuration.
        $confetti_settings = $this->config('confetti.settings');
        if (!$form_state->has('maximum_confetti_urls')) {
            $form_state->set('maximum_confetti_urls', 1);
        }

        $form['page_details'] = [
            '#type' => 'table',
            '#open' => TRUE,
            '#tree' => TRUE,
            '#header' => [
                $this->t('Page Details'),
            ],
            '#title' => $this->t('Confetti pages'),
            '#description' => $this->t('In the following fields, write path after the domain to show confetti on the page.') . '<br>' . $this->t('For example, write: "/example", when the full URL is "https://www.yourpage.com/example"'),
            '#prefix' => '<div id="confetti-details-wrapper">',
            '#suffix' => '</div>',
        ];

        // Obtain the existing confetti page URLs.
        if ($confetti_urls = $confetti_settings->get('confetti_urls')) {
            foreach ($confetti_urls as $value) {
                $form['page_details'][] = [
                    'confetti_url' => [
                        '#type' => 'textfield',
                        '#value' => $value,
                    ],
                ];
            }
        }

        // Fields for the new domain shop_details.
        for ($i = 0; $i < $form_state->get('maximum_confetti_urls'); $i++) {
            $form['page_details'][] = [
                'confetti_url' => [
                    '#type' => 'textfield',
                ],
            ];
            if ($i === 0 && $form_state->get('maximum_confetti_urls') === 1) {
                $form['page_details'][0]['confetti_url']['#title'] = $this->t('URL subpath');
            }
        }

        $form['add'] = [
            '#type' => 'submit',
            '#value' => $this->t('Add another'),
            '#submit' => ['::addAnotherSubmit'],
            '#ajax' => [
                'callback' => '::ajaxAddAnother',
                'wrapper' => 'confetti-details-wrapper',
            ],
        ];
        // Submit.
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save'),
            '#button_type' => 'primary',
        ];

        return $form;
    }

    /**
     * Summary of validateForm.
     *
     * @param array $form
     *   The form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form_state.
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        parent::validateForm($form, $form_state);
        if ($confetti_urls = $form_state->getValue('page_details')) {
            foreach ($confetti_urls as $confetti_url) {
                if (is_array($confetti_url) && !empty($confetti_url['confetti_url'])) {
                    // Set up the confetti URL in the settings.
                    $confetti_url_paths[] = $confetti_url['confetti_url'];
                }
            }
        }
        if (!empty($confetti_url_paths)) {
            foreach ($confetti_url_paths as $confetti_url) {
                try {
                    Url::fromUserInput($confetti_url);
                }
                catch (\Exception $e) {
                    $form_state->setErrorByName('page_details', $this->t('URL Sub-path need to start with /.'));
                }
            }
        }
        else {
            $form_state->setErrorByName('page_details', $this->t('URL Sub-path is empty.'));
        }
    }

    /**
     * SubmitForm.
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $confetti_settings = $this->config('confetti.settings');
        $confetti_url_paths = [];
        // Clear the cache for the already existing pages (if any are removed).
        $confetti_cache_url_paths = $confetti_settings->get('confetti_urls');
        if (!empty($confetti_cache_url_paths)) {
            foreach ($confetti_cache_url_paths as $confetti_url) {
                $url = Url::fromUserInput($confetti_url);
                if ($url instanceof Url) {
                    if ($url->isRouted() && !empty($url->getRouteParameters()['node'])) {
                        $nid = $url->getRouteParameters()['node'];
                        /** @var \Drupal\node\Entity\Node $node */
                        $node = $this->nodeStorage->load($nid);
                        if ($node instanceof NodeInterface) {
                            $tags = $node->getCacheTags();
                            $this->cacheTagsInvalidator->invalidateTags($tags);
                        }
                    }
                }
            }
        }
        if ($confetti_urls = $form_state->getValue('page_details')) {
            foreach ($confetti_urls as $confetti_url) {
                if (is_array($confetti_url) && !empty($confetti_url['confetti_url'])) {
                    // Set up the confetti URL in the settings.
                    $confetti_url_paths[] = $confetti_url['confetti_url'];
                }
            }
        }
        foreach ($confetti_url_paths as $confetti_url) {
            $url = Url::fromUserInput($confetti_url);
            if ($url instanceof Url) {
                if ($url->isRouted()) {
                    $nid = $url->getRouteParameters()['node'];
                    /** @var \Drupal\node\Entity\Node $node */
                    $node = $this->nodeStorage->load($nid);
                    if ($node instanceof NodeInterface) {
                        $tags = $node->getCacheTags();
                        $this->cacheTagsInvalidator->invalidateTags($tags);
                    }
                }
            }
        }
        $confetti_settings->set('confetti_urls', $confetti_url_paths);
        $confetti_settings->save();
    }

    /**
     * Submit callback for adding a new domain field.
     */
    public function addAnotherSubmit(array $form, FormStateInterface $form_state) {
        $form_state->set('maximum_confetti_urls', $form_state->get('maximum_confetti_urls') + 1);
        $form_state->setRebuild(TRUE);
    }

    /**
     * Ajax callback for adding another domain redirect.
     *
     * @param array $form
     *   The form structure.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state.
     *
     * @return array
     *   The new domain redirect form part.
     */
    public function ajaxAddAnother(array $form, FormStateInterface $form_state) {
        return $form['page_details'];
    }

}
