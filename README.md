# Confetti

This module provides confetti effect on any page. In the configuration form,
you can add the URL sub-path for any page.
It is based on [canvas-confetti](https://github.com/catdad/canvas-confetti),
uses its as an external JS library.

Customization is optional, one example is given in the module.


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

For configuration settings goto `/admin/confetti`.


## Maintainers

- [Ivan Ginovski (Ginovski)](https://www.drupal.org/u/ginovski)
