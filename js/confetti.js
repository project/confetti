(function ($, drupalSettings) {

    Drupal.behaviors.Confetti = {
        attach: function (context) {
            var self = this;

            setTimeout(() => {
                var time = 6;
                if ($(document).width() < 742) {
                    time = 4;
                }
                var end = Date.now() + time * 1000;
                var colors = ["#81386d", "#a5c200"];

                (function frame() {
                    if (typeof window.confetti === "function") {
                        window.confetti({
                            particleCount: 2.5,
                            angle: 60,
                            spread: 45,
                            origin: { x: 0 },
                            colors: colors,
                        });
                        window.confetti({
                            particleCount: 2.5,
                            angle: 120,
                            spread: 45,
                            origin: { x: 1 },
                            colors: colors,
                        });

                        if (Date.now() < end) {
                            requestAnimationFrame(frame);
                        }
                    }
                })();
            }, 500);
        }
    }
}(jQuery, drupalSettings));
